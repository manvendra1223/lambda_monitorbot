import requests
import json
from settings import TOKEN, DEFAULT_CHANNEL, SLACK_BASE_URL

token =TOKEN

def lambda_function(event, context):
    # TODO implement
    status = event[0]["event"]
    url = event[0]["check"]["url"]
    status_code = event[0]["check"]["last_status"]
    res = ''
    msg = ''
    try:
        if status == "check.down":
            msg = "Website {0} is currently down\nReturned Status code {1}".format(url,status_code)
            responce = send_slack_notification(msg)
            if responce["ok"]:
                return json.dumps(msg)
            else:
                return json.dumps(responce)
    except KeyError:
        return json.dumps("Invalid request")

    return json.dumps("No Errors Found")


def send_slack_notification(text):
    attachments = [{"text":text, "color":"danger", "title":"Site Down Alert" }]
    attachments = json.dumps(attachments)
    payload = {"token":token, "channel":DEFAULT_CHANNEL, "attachments":attachments}
    responce = requests.post(SLACK_BASE_URL+"/chat.postMessage",
                                data=payload)
    responce.raise_for_status()
    return responce.json() 